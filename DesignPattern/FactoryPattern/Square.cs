﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPattern.FactoryPattern
{
    public class Square : Shape
    {
        public Shape Draw()
        {
            Console.WriteLine("This is Square Object!");
            return this;
        }
        public void Side()
        {
            Console.WriteLine("Four Side in the Square!");
        }
    }
}
