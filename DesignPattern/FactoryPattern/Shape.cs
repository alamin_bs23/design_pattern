﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPattern.FactoryPattern
{
    public interface Shape
    {
        Shape Draw();
        void Side();
    }
}
