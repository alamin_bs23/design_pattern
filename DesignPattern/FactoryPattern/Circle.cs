﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPattern.FactoryPattern
{
    public class Circle : Shape
    {
        public Shape Draw()
        {
            Console.WriteLine("This is Circle Object!");
            return this;
        }

        public void Side()
        {
            Console.WriteLine("Three side in the circle.");
        }
    }
}
