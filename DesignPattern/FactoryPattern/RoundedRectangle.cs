﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPattern.FactoryPattern
{
    public class RoundedRectangle : Shape
    {
        public Shape Draw()
        {
            Console.WriteLine("This is Rounded rectangle");
            return this;
        }

        public void Side()
        {
            Console.WriteLine("There is four side in Rounded Rectangle!");
        }
    }
}
