﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPattern.FactoryPattern
{
    public class Rectangle : Shape
    {
        public Shape Draw()
        {
            Console.WriteLine("This is rectangle object!");
            return this;
        }

        public void Side()
        {
            Console.WriteLine("Four Side in the rectangle!");
        }
    }
}
