﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPattern.AdapterPattern
{
    public class AudioPlayer : MediaPlayer
    {
        MediaAdapter mediaAdapter;
        public void play(string audioType, string fileName)
        {
            if (audioType.Equals("mp3", StringComparison.InvariantCultureIgnoreCase))
            {
                Console.WriteLine("Playing mp3 file. filename {0}", fileName);
            }
            else if(audioType.Equals("vlc",StringComparison.InvariantCultureIgnoreCase) ||
                    audioType.Equals("mp4", StringComparison.InvariantCultureIgnoreCase))
            {
                mediaAdapter = new MediaAdapter(audioType);
                mediaAdapter.play(audioType, fileName);
            }
            else
            {
                Console.WriteLine("Invalid media. " + audioType + " format not supported");
            }
        }
    }
}
