﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPattern.AdapterPattern
{
    public class VlcPlayer : AdvancedMediaPlayer
    {
        public void playMp4(string fileName)
        {
            Console.WriteLine("Don't Support....", fileName);
        }

        public void playVlc(string fileName)
        {
            Console.WriteLine("Playing vlc file. Name {0}", fileName);
        }
    }
}
