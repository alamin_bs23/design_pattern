﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPattern.AdapterPattern
{
    public class Mp4Player : AdvancedMediaPlayer
    {
        public void playMp4(string fileName)
        {
            Console.WriteLine("Playing Mp4 file. Filename : {0}", fileName);
        }

        public void playVlc(string fileName)
        {
            // Do nothing here....
        }
    }
}
