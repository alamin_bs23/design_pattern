﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPattern.AdapterPattern
{
    public class MediaAdapter : MediaPlayer
    {
        AdvancedMediaPlayer advancedMediaPlayer;
        public MediaAdapter(string audioType)
        {
            if (audioType.Equals("Vlc", StringComparison.InvariantCultureIgnoreCase))
            {
                advancedMediaPlayer = new VlcPlayer();
            }
            else if (audioType.Equals("mp4", StringComparison.InvariantCultureIgnoreCase))
            {
                advancedMediaPlayer = new Mp4Player();
            }
        }
        public void play(string audioType, string fileName)
        {
            if (audioType.Equals("vlc", StringComparison.InvariantCultureIgnoreCase))
            {
                advancedMediaPlayer.playVlc(fileName);
            }
            else if(audioType.Equals("mp4" , StringComparison.InvariantCultureIgnoreCase))
            {
                advancedMediaPlayer.playMp4(fileName);
            }
        }
    }
}
