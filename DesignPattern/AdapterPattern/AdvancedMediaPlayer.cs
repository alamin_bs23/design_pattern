﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPattern.AdapterPattern
{
    public interface AdvancedMediaPlayer
    {
        public void playVlc(string fileName);
        public void playMp4(string fileName);
    }
}
