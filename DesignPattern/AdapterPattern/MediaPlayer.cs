﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPattern.AdapterPattern
{
    public interface MediaPlayer
    {
        public void play(String audioType, String fileName);
    }
}
