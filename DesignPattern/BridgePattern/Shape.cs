﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPattern.BridgePattern
{
    public abstract class Shape
    {
        protected DrawAPI drawAPI;
        protected Shape(DrawAPI drawApi)
        {
            this.drawAPI = drawApi;
        }
        public abstract void draw();
    }
}
