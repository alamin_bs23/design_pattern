﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPattern.BridgePattern
{
    public interface DrawAPI
    {
        public void drawCircle(int radius, int x, int y);
    }
}
