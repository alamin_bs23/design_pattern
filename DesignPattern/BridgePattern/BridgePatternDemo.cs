﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPattern.BridgePattern
{
    public static class BridgePatternDemo
    {
        public static void Demo()
        {
            Shape redCircle = new Circle(100, 100, 10, new RedCircle());
            Shape greenCircle = new Circle(100, 100, 12, new GreenCircle());
            redCircle.draw();
            greenCircle.draw();
        }
    }
}
