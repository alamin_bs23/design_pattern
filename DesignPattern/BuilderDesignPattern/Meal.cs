﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPattern.BuilderDesignPattern
{
    public class Meal
    {
        private List<Item> items = new List<Item>();

        public void addItem(Item item)
        {
            items.Add(item);
        }
        public float getCost()
        {
            float cost = 0.0f;
            foreach(var item in items)
            {
                cost += item.price();
            }
            return cost;
        }
        public void showItems()
        {
            foreach(var item in items)
            {
                Console.WriteLine("Name : {0}", item.name());
                Console.WriteLine("Packge : {0}", item.Packing().pack());
                Console.WriteLine("Price : {0}", item.price());
                Console.WriteLine("-------------------------------------------");
            }
        }
    }
}
