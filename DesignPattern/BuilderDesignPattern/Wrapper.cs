﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPattern.BuilderDesignPattern
{
    public class Wrapper : Packing
    {
        public string pack()
        {
            return "Wrapper";
        }
    }
}
