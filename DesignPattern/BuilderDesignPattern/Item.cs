﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPattern.BuilderDesignPattern
{
    public interface Item
    {
        public String name();
        public Packing Packing();
        public float price();
    }
}
