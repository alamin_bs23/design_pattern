﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPattern.BuilderDesignPattern
{
    public abstract class Burger : Item
    {
        public abstract string name();

        public Packing Packing()
        {
            return new Wrapper();
        }

        public abstract float price();
    }
}
