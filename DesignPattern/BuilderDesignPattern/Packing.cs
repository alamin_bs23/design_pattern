﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPattern.BuilderDesignPattern
{
    public interface Packing
    {
        public String pack();
    }
}
