﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPattern.BuilderDesignPattern
{
    public abstract class ColdDrink : Item
    {
        public abstract string name();

        public Packing Packing()
        {
            return new Bottle();
        }

        public abstract float price();
    }
}
