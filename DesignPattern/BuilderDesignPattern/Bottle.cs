﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPattern.BuilderDesignPattern
{
    public class Bottle : Packing
    {
        public string pack()
        {
            return "Bottle";
        }
    }
}
