﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPattern.SingelTonDesignPattern
{
    public sealed class Singelton
    {
        private static int counter = 0;
        private static Singelton instance = null;
        public string value = "Hello";
        public static Singelton GetInstance
        {
            get
            {
                if (instance == null)
                    instance = new Singelton();
                return instance;
            }
        }
        private Singelton()
        {
            counter++;
            Console.WriteLine("Counter Value {0}", counter);
        }
        public void PrintDetails(string message)
        {
            Console.WriteLine("{0}",value);
        }
    }
}
