﻿using DesignPattern.AbstractFactoryPattern;
using DesignPattern.AdapterPattern;
using DesignPattern.BridgePattern;
using DesignPattern.BuilderDesignPattern;
using DesignPattern.EventHandler;
using DesignPattern.FactoryPattern;
using DesignPattern.NewFolder;
using DesignPattern.ObserverDesignPattern;
using DesignPattern.SingelTonDesignPattern;
using System;

namespace DesignPattern
{
    class Program
    {
        static void Main(string[] args)
        {

            #region Factory Pattern
            //ShapeFactory shapeFactory = new ShapeFactory();
            //shapeFactory.GetShape("CIRCLE").Draw().Side();
            //shapeFactory.GetShape("RECTANGLE").Draw().Side();
            #endregion Ended Factory Pattern

            #region Abstract Factory Pattern
            //ProducerFactory.getFactory(true).getShape("RECTANGLE").Draw();
            //ProducerFactory.getFactory(false).getShape("RECTANGLE").Draw().Side();
            #endregion Ended Abstract Factory Pattern

            #region Singelton Design Patter
            //Singelton singelton = Singelton.GetInstance;
            //singelton.value = "Not Okay";
            //singelton.PrintDetails(singelton.value);
            //Singelton another = Singelton.GetInstance;
            //another.PrintDetails(another.value);
            #endregion

            #region Builder Patter Demo

            //MealBuilder mealBuilder = new MealBuilder();
            //Meal vegMeal = mealBuilder.prepareVegMeal();
            //vegMeal.showItems();
            //Meal nonVegMeal = mealBuilder.prepareNonVegMeal();
            //Console.WriteLine();
            //nonVegMeal.showItems();

            #endregion


            #region Adapter Pattern DEMO

            //AudioPlayer audioPlayer = new AudioPlayer();

            //audioPlayer.play("mp3", "beyond the horizon.mp3");
            //audioPlayer.play("mp4", "alone.mp4");
            //audioPlayer.play("vlc", "far far away.vlc");
            //audioPlayer.play("avi", "mind me.avi");


            #endregion

            #region Bridge Pattern 

            //BridgePatternDemo.Demo();

            #endregion

            //ObserverPatternDemo.DemoCheck();
            //ObserverPatternDemo.ExampleDemoCheck();

            //EventPublisher eventPublisher = new EventPublisher();
            //eventPublisher.RaiseEvent += EventPublisher_RaiseEvent;
            //var test = new TestHandle(eventPublisher);
            //eventPublisher.startProcess();
            //Class1.print();

            Car hondaCity = new HondaCity(11);
            hondaCity.printCar("HondaCity");

        }

        public abstract class Car
        {
            public Car(bool isSedan, string seats)
            {
                this.isSedan = isSedan;
                this.seats = seats;
            }
            private bool isSedan;
            private string seats;
            public bool getIsSedan()
            {
                return this.isSedan;
            }
            public string getSeats()
            {
                return this.seats; ;
            }
            public abstract string getMileage();
            public abstract void printCar(string name);
        }

        public class WagonR : Car
        {
            private int mileage;
            private string name;
            public WagonR(int mileage, bool isSedan = false, string seats = "is 4-seater") : base(isSedan, seats)
            {
                this.mileage = mileage;
                this.name = "WagonR";
            }
            public override string getMileage()
            {
                string mileage = "A " + this.name + " is " + (this.getIsSedan() ? "" : "not ") + "Sedan, " + (this.getSeats()) + " and has a mileage of around " + this.mileage.ToString() + " kmpl.";
                return mileage;
            }

            public override void printCar(string name)
            {
                var mileAge = this.getMileage();
                Console.WriteLine(mileAge);
            }
        }

        public class HondaCity : Car
        {
            private int mileage;
            private string name;
            public HondaCity(int mileage, bool isSedan = true, string seats = "is 4-seater") : base(isSedan, seats)
            {
                this.mileage = mileage;
                this.name = "HondaCity";
            }
            public override string getMileage()
            {
                string mileage = "A " + this.name + " is " + (this.getIsSedan() ? "" : "not ") + "Sedan, " + (this.getSeats()) + " and has a mileage of around " + this.mileage.ToString() + " kmpl.";
                return mileage;
            }

            public override void printCar(string name)
            {
                var mileAge = this.getMileage();
                Console.WriteLine(mileAge);
            }
        }
        public class InnovaCrysta : Car
        {
            private int mileage;
            private string name;
            public InnovaCrysta(int mileage, bool isSedan = true, string seats = "is 4-seater") : base(isSedan, seats)
            {
                this.mileage = mileage;
                this.name = "InnovaCrysta";
            }
            public override string getMileage()
            {
                string mileage = "A " + this.name + " is " + (this.getIsSedan() ? "" : "not ") + "Sedan, " + (this.getSeats()) + " and has a mileage of around " + this.mileage.ToString() + " kmpl.";
                return mileage;
            }

            public override void printCar(string name)
            {
                var mileAge = this.getMileage();
                Console.WriteLine(mileAge);
            }
        }
    }
}
