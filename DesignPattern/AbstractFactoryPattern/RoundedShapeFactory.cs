﻿using DesignPattern.FactoryPattern;
using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPattern.AbstractFactoryPattern
{
    public class RoundedShapeFactory : AbstractFactory
    {
        public override Shape getShape(string ShapeType)
        {
            if (ShapeType.Equals("RECTANGLE"))
            {
                return new RoundedRectangle();
            }
            else return null;
        }
    }
}
