﻿using DesignPattern.FactoryPattern;
using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPattern.AbstractFactoryPattern
{
    public abstract class AbstractFactory
    {
        public abstract Shape getShape(string ShapeType);
    }
}
