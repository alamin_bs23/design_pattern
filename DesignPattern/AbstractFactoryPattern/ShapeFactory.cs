﻿using DesignPattern.FactoryPattern;
using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPattern.AbstractFactoryPattern
{
    public class ShapeFactory : AbstractFactory
    {
        public override Shape getShape(string ShapeType)
        {
            if (ShapeType.Equals("RECTANGLE"))
            {
                return new Rectangle();
            }
            else if (ShapeType.Equals("CIRCLE"))
            {
                return new Circle();
            }
            else if (ShapeType.Equals("SQUARE"))
            {
                return new Square();
            }
            else return null;
        }
    }
}
