﻿using DesignPattern.ObserverDesignPattern.AnotherExample;
using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPattern.ObserverDesignPattern
{
    public class ObserverPatternDemo
    {
       
        public static void DemoCheck()
        {
            Subject subject = new Subject();
            new HexaObserver(subject);
            new OctalObserver(subject);
            new BinaryObserver(subject);

            Console.WriteLine("First state change:15");
            subject.setState(15);
            Console.WriteLine("Second state change:10");
            subject.setState(10);
        }
        
        public static void ExampleDemoCheck()
        {
            //var subject = new AnotherExample.Subject();
            //var observerA = new ConcreteObserverA();
            //subject.Attach(observerA);

            //var observerB = new ConcreteObserverB();
            //subject.Attach(observerB);

            //subject.SomeBusinessLogic();
            //subject.SomeBusinessLogic();

            //subject.Detach(observerB);

            //subject.SomeBusinessLogic();

        }

    }
}
