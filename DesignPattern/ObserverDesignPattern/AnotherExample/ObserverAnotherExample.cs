﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace DesignPattern.ObserverDesignPattern.AnotherExample 
{
    public interface IObserver
    {
        public void update(ISubject subject);
    }
    public interface ISubject
    {
        void Attach(IObserver observer);
        void Detach(IObserver observer);
        void Notify();
    }
    public class Subject : ISubject
    {
        public int State { get; set; } = 0;
        private List<IObserver> _observers = new List<IObserver>();

        public void Attach(IObserver observer)
        {
            Console.WriteLine("Subject: Attached an observer.");
            this._observers.Add(observer);
        }

        public void Detach(IObserver observer)
        {
            this._observers.Remove(observer);
            Console.WriteLine("Subject: Detached an observer.");
        }

        public void Notify()
        {
            Console.WriteLine("Subject: Notifying observers...");

            foreach (var observer in _observers)
            {
                observer.update(this);
            }
        }

        // Usually, the subscription logic is only a fraction of what a Subject
        // can really do. Subjects commonly hold some important business logic,
        // that triggers a notification method whenever something important is
        // about to happen (or after it).
        public void SomeBusinessLogic()
        {
            Console.WriteLine("\nSubject: I'm doing something important.");
            this.State = new Random().Next(0, 10);
            Thread.Sleep(15);

            Console.WriteLine("Subject: My state has just changed to: " + this.State);
            this.Notify();
        }
    }


    public class ConcreteObserverA : IObserver
    {
        protected Subject subject;
        public ConcreteObserverA(ISubject subject)
        {
            this.subject = (Subject)subject;
            this.subject.Attach(this);
        }

        public void update(ISubject subject)
        {
            throw new NotImplementedException();
        }
    }

    //class ConcreteObserverB : ConcreteObserverA
    //{
    //    public void update(ISubject subject)
    //    { 
    //        if ((subject as Subject).State == 0 || (subject as Subject).State >= 2)
    //        {
    //            Console.WriteLine("ConcreteObserverB: Reacted to the event.");
    //        }
    //    }
    //}

}
