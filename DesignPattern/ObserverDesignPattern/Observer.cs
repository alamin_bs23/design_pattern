﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPattern.ObserverDesignPattern
{
    public abstract class Observer
    {
        protected Subject subject;
        public abstract void update();
    }
}
