﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPattern.ObserverDesignPattern
{
    public class HexaObserver : Observer
    {
        public HexaObserver(Subject subject)
        {
            this.subject = subject;
            this.subject.attach(this);
        }
        public override void update()
        {
            Console.WriteLine("Hexa String: " + subject.getState());
        }
    }
}
