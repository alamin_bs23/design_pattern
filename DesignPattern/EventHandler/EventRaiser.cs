﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPattern.EventHandler
{
    public abstract class EventRaiser
    {
        protected EventPublisher eventPublisher;
        public EventRaiser(EventPublisher eventPublisher)
        {
            this.eventPublisher = eventPublisher;
            this.RegisterEvent();
        }
        public void RegisterEvent()
        {
            eventPublisher.RaiseEvent += HandleAsync;
        }

        public abstract void HandleAsync(object sender, string e);
    }
}
