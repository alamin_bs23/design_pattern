﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPattern.EventHandler
{
    public class EventPublisher
    {
        public event EventHandler<string> RaiseEvent;

        public void startProcess() 
        {
            OnRaiseTheEvent("Hello Test");
        }

        public void OnRaiseTheEvent(string hello)
        {
            RaiseEvent?.Invoke(this, hello);
        }
    }
}
