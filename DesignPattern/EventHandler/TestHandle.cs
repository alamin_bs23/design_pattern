﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPattern.EventHandler
{
    public class TestHandle : EventRaiser
    {
        public TestHandle(EventPublisher eventPublisher) : base(eventPublisher)
        {

        }
        public override void HandleAsync(object sender, string e)
        {
            Console.WriteLine("Test Handle Is Called");
        }
    }
}
